class Tab {
  constructor(tabsData) {
    this.tabsData = tabsData;
  }

  createCompotent(tabsData) {
    const tabs = document.createElement('div');
    const tabItems = document.createElement('div');
    tabItems.classList.add('tab__items');
    this.currentTabContent = document.createElement('div');

    tabsData.forEach((data) => {
      const tabItem = document.createElement('div');
      tabItem.classList.add('tab__item');
      tabItem.innerText = data.title;
      tabItem.tabIndex = 1;
      tabItems.appendChild(tabItem);
    });

    tabItems.children[tabsData.length - 1].tabIndex = 0;
    this.currentTabContent.innerText = tabsData[0].title;
    this.currentTabItem = tabItems.children[0];

    this.currentTabItem.classList.add('_current');

    tabs.appendChild(tabItems);
    tabs.appendChild(this.currentTabContent);

    return tabs;
  }

  getDataByTitle(title) {
    for(let i = 0; i < this.tabsData.length; i++) {
      if(title === this.tabsData[i].title) {
        return this.tabsData[i].data;
      }
    }

    return undefined;
  }

  initEventListener() {
    document.addEventListener('mousedown', (e) => {
      if (e.target.classList.contains("tab__item")) {
        this.currentTabContent.innerText = this.getDataByTitle(e.target.innerText);
        this.currentTabItem.classList.remove('_current');
        this.currentTabItem = e.target;
        this.currentTabItem.classList.add('_current');
        e.preventDefault();
      }
    });
  }

  render() {
    this.initEventListener()
    return this.createCompotent(this.tabsData);;
  }
}

// Входные данные
const tab = new Tab([
  {
    title: 'Общая информация',
    data: `Какая-то личная информация о пользователе`
  },
  {
    title: 'История',
    data: `Какая-то история действий пользователя`
  },
  {
    title: 'Друзья',
    data: `Какие-то люди`
  }
])

// Вставляем Таб на страницу и вызываем метод рэендер
document.getElementById('tab').appendChild(tab.render())


const user = {
  name: "John",
  surname: "Doe",
  age: 20,
  company: {
    name: "Microsoft",
    location: "Redmond",
    department: {
      name: "Marketing",
    },
  },
};
